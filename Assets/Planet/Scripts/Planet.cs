using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Planet : MonoBehaviour
{
    private float m_rotationSpeed;

    [SerializeField]
    private float m_temperature;

    private AudioSource M1;
    private Rigidbody m_rigidbody;
    private Transform cam;
    private Vector3 randVec;

    public static Planet instance;

    private void Awake()
    {
        instance = this;
        m_rigidbody = GetComponent<Rigidbody>();
        cam = GameObject.FindGameObjectWithTag("MainCamera").transform;
        M1 = this.GetComponent<AudioSource>();
        randVec = new Vector3(0, 0, 0);
        m_rotationSpeed = 10;
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.LeftArrow))
            LowerSpeed();
        if (Input.GetKeyUp(KeyCode.RightArrow))
            RaiseSpeed();

        float newPitch = Mathf.Abs(m_rotationSpeed * 0.1F);

        if (newPitch > 2.0f)
            newPitch = 2.0f;
        else if (newPitch < -2.0f)
            newPitch = -2.0f;

        M1.pitch = Mathf.Lerp(M1.pitch, newPitch, Time.deltaTime);
    }

    private void LateUpdate()
    {
        //Turns Planet ============
        m_rigidbody.angularVelocity = new Vector3(0, 0, -(m_rotationSpeed / 4));

        //Camera Shake ============
        if (m_rotationSpeed > 16 || m_rotationSpeed < -6)
        {
            randVec = Random.insideUnitSphere * (m_rotationSpeed * 0.00006F);
            cam.localRotation = new Quaternion(randVec.x, randVec.y, randVec.z, 1);
        }
        else
        {
            if (cam.localRotation.y != 0 && cam.localRotation.x != 0 && cam.localRotation.z != 0)
                cam.localRotation = new Quaternion(0, 0, 0, 1);
        }
    }

    public void RaiseSpeed()
    {
        if (m_rotationSpeed < 30)
        {
            UIManager.instance.UpdateSpeedText((int)m_rotationSpeed + 1);
            m_rotationSpeed += 1;
        }
    }
    public void LowerSpeed()
    {
        if (m_rotationSpeed > -40)
        {
            UIManager.instance.UpdateSpeedText((int)m_rotationSpeed - 1);
            m_rotationSpeed -= 1;
        }
    }

    public float GetSpeed() { return m_rotationSpeed; }

}