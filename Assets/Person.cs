using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(FixedJoint))]
public class Person : MonoBehaviour
{
    private Renderer m_renderer;
    private Rigidbody m_rigidbody;
    private JointInfo m_jointInfo;
    private Vector3 m_originPosition;
    private Quaternion m_originRotation;

    private void Awake()
    {
        SpawnManager.peopleList.Add(this);
        m_renderer = GetComponentInChildren<Renderer>();
        m_rigidbody = GetComponent<Rigidbody>();
        var defaultJointSettings = GetComponent<Joint>();
        m_jointInfo = new JointInfo
        {
            connectedBody = defaultJointSettings.connectedBody,
            breakForce = defaultJointSettings.breakForce,
            breakTorque = defaultJointSettings.breakTorque,
            enableCollision = defaultJointSettings.enableCollision,
            enablePreprocessing = defaultJointSettings.enablePreprocessing,
            massScale = defaultJointSettings.massScale,
            connectedMassScale = defaultJointSettings.connectedMassScale
        };
        m_originPosition = transform.position;
        m_originRotation = transform.rotation;

        gameObject.SetActive(false);
    }

    private void Update()
    {
        ResetPersonIfOutOfCameraBounds();
    }

    private void ResetPersonIfOutOfCameraBounds()
    {
        // If there renderer is off screen then reset their component details
        if (!m_renderer.isVisible)
        {
            gameObject.SetActive(false);
            // set to start position & rotation
            transform.position = m_originPosition;
            transform.rotation = m_originRotation;

            // At this point their Joint should have broke from the world
            // so we need to add it back and fill it with the details
            // that it started with at the launch of the game.
            if (!GetComponent<FixedJoint>())
            {
                var j = gameObject.AddComponent<FixedJoint>();
                j.connectedBody = m_jointInfo.connectedBody;
                j.breakForce = m_jointInfo.breakForce;
                j.breakTorque = m_jointInfo.breakTorque;
                j.enableCollision = m_jointInfo.enableCollision;
                j.enablePreprocessing = m_jointInfo.enablePreprocessing;
                j.massScale = m_jointInfo.massScale;
                j.connectedMassScale = m_jointInfo.connectedMassScale;

            }
        }
    }

    private void OnEnable()
    {
        // set to start position & rotation
        transform.position = m_originPosition;
        transform.rotation = m_originRotation;

        // At this point their Joint should have broke from the world
        // so we need to add it back and fill it with the details
        // that it started with at the launch of the game.
        if (!GetComponent<FixedJoint>())
        {
            var j = gameObject.AddComponent<FixedJoint>();
            j.connectedBody = m_jointInfo.connectedBody;
            j.breakForce = m_jointInfo.breakForce;
            j.breakTorque = m_jointInfo.breakTorque;
            j.enableCollision = m_jointInfo.enableCollision;
            j.enablePreprocessing = m_jointInfo.enablePreprocessing;
            j.massScale = m_jointInfo.massScale;
            j.connectedMassScale = m_jointInfo.connectedMassScale;

        }
    }

    private void OnDisable()
    {
        // set to start position & rotation
        transform.position = m_originPosition;
        transform.rotation = m_originRotation;

        // At this point their Joint should have broke from the world
        // so we need to add it back and fill it with the details
        // that it started with at the launch of the game.
        if (!GetComponent<FixedJoint>())
        {
            var j = gameObject.AddComponent<FixedJoint>();
            j.connectedBody = m_jointInfo.connectedBody;
            j.breakForce = m_jointInfo.breakForce;
            j.breakTorque = m_jointInfo.breakTorque;
            j.enableCollision = m_jointInfo.enableCollision;
            j.enablePreprocessing = m_jointInfo.enablePreprocessing;
            j.massScale = m_jointInfo.massScale;
            j.connectedMassScale = m_jointInfo.connectedMassScale;

        }
    }
}

public class JointInfo
{
    public Rigidbody connectedBody;
    public float breakForce;
    public float breakTorque;
    public bool enableCollision;
    public bool enablePreprocessing;
    public float massScale;
    public float connectedMassScale;
}