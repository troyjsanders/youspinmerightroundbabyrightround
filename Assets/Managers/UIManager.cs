using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIManager : MonoBehaviour
{
    public GameObject SpeedTextObj;
    private TextMeshProUGUI SpeedText;

    public List<Sprite> Clouds = new List<Sprite>();

    public static UIManager instance;

    private void Awake()
    {
        instance = this;
        SpeedText = SpeedTextObj.GetComponent<TextMeshProUGUI>();
        UpdateSpeedText(10);
    }

    public void UpdateSpeedText(int speed)
    {
        SpeedText.text = speed.ToString();
    }
}
