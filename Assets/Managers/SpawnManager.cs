using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public static List<Person> peopleList = new List<Person>();

    private int index = 0;

    private void Start()
    {
        InvokeRepeating("SpawnPerson", 0, .5f);
    }

    private void SpawnPerson()
    {
        if(peopleList?.Count > 0)
        {
            if (index >= peopleList.Count)
            {
                index = 0;
            }

            peopleList[index].gameObject.SetActive(true);
            if (index >= peopleList.Count)
            {
                index = 0;
            }
            else
            {
                index++;
            }
        }
    }
}