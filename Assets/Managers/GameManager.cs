using UnityEngine;

public class GameManager : MonoBehaviour
{
    private UIManager m_uiManager;
    private SpawnManager m_spawnManager;

    private void Awake()
    {
        InitializeSystems();
    }

    private void InitializeSystems()
    {
        InitializeUISystem();
        InitializeSpawnSystem();
    }

    internal void InitializeUISystem()
    {
        if (m_uiManager == null)
        {
            m_uiManager = GetComponentInChildren<UIManager>();
        }

        if (m_uiManager == null)
        {
            throw new System.Exception($"{nameof(UIManager)} is missing or is null");
        }
    }

    internal void InitializeSpawnSystem()
    {
        if (m_spawnManager == null)
        {
            m_spawnManager = GetComponentInChildren<SpawnManager>();
        }

        if (m_spawnManager == null)
        {
            throw new System.Exception($"{nameof(SpawnManager)} is missing or is null");
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}