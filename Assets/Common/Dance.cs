using UnityEngine;

public class Dance : MonoBehaviour
{
    [SerializeField]
    private float m_pulseRate = 1.0f;
    [SerializeField]
    private float m_amplitude = 1.0f;

    private Vector3 m_startScale = Vector3.one;

    private Vector3 m_rotationPointA;
    private Vector3 m_rotationPointB;

    [SerializeField]
    private float m_rotationSpeed = 1.0f;
    [SerializeField]
    private float m_yAngle = 30.0f;


    private float m_speed;

    public SpriteRenderer rend;
    private bool isCloud;
    private int cloudInd;
    private float emotionalDamage;

    private void OnEnable()
    {
        m_startScale = transform.localScale;

        //Get current position then add 90 to its Y axis
        m_rotationPointA = transform.eulerAngles + new Vector3(0f, 0, m_yAngle);

        //Get current position then substract -90 to its Y axis
        m_rotationPointB = transform.eulerAngles + new Vector3(0f, 0, -m_yAngle);

        rend = this.GetComponent<SpriteRenderer>();
        isCloud = (rend.sprite.name.Contains("cloud")) ? true : false;
        emotionalDamage = (float)Random.Range(1, 5);
    }

    private void LateUpdate()
    {
        Scale();
        Rotation();
        SpriteSwap();
    }

    private void Scale()
    {
        Vector3 vec = new Vector3(m_amplitude * Mathf.Sin(Time.time * m_pulseRate) + m_startScale.x, m_amplitude * Mathf.Sin(Time.time * m_pulseRate) + m_startScale.y, m_amplitude * Mathf.Sin(Time.time * m_pulseRate) + m_startScale.z);
        transform.localScale = vec;
    }

    private void Rotation()
    {
        //PingPong between 0 and 1
        float time = Mathf.PingPong(Time.time * m_rotationSpeed, 1);
        transform.eulerAngles = Vector3.Lerp(m_rotationPointA, m_rotationPointB, time);
    }

    private void SpriteSwap()
    {
        if (isCloud)
        {
            m_speed = Mathf.Abs(Planet.instance.GetSpeed());
            if (m_speed > 16 || m_speed <= -10)
            {
                emotionalDamage -= Time.deltaTime;

                if (emotionalDamage <= 0)
                {
                    cloudInd = (cloudInd == 1) ? 2 : 1;
                    rend.sprite = UIManager.instance.Clouds[cloudInd];
                    emotionalDamage = (float)Random.Range(1, 3);
                }
            }
            else if (m_speed < -5 || m_speed > 12)
            {
                emotionalDamage -= Time.deltaTime;

                if (emotionalDamage <= 0)
                {
                    cloudInd = (cloudInd == 0) ? 1 : 0;
                    rend.sprite = UIManager.instance.Clouds[cloudInd];
                    emotionalDamage = (float)Random.Range(1, 4);
                }
            }
            else
            {
                if (cloudInd != 0)
                {
                    rend.sprite = UIManager.instance.Clouds[0];
                    cloudInd = 0;
                }
            }
        }
    }
}